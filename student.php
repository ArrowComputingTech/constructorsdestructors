<?php
  class Student {
    public $name;
    public $class;

    function __construct($name = "", $class = "") {
      $this->name = $name;
      $this->class = $class;
      echo "This is the constructor.<br>";
    }

    function __destruct() {
      echo "This is the destructor.<br>";
    }

    function printDetails() {
      echo "{$this->name} is studying in {$this->class} class.<br>";
    }

  }
  $student = new Student("John Smith", "Computer Science");
  $student->printDetails();

?>
